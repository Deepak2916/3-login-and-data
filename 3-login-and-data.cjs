
// NOTE: Do not change the name of this file

// NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

const fs = require('fs')
const path = require('path')


// Q1. Create 2 files simultaneously (without chaining).

function createTwoFiles() {
    const file1Path = path.join(__dirname, 'file1.txt')
    const file2Path = path.join(__dirname, 'file2.txt')

    const promis1 = new Promise((resolve, reject) => {
        fs.writeFile(file1Path, 'file1.txt created', (err) => {
            if (err) {
                reject(err)
            } else {
                resolve('file1.txt')
            }
        })
    })
    const promis2 = new Promise((resolve, reject) => {
        fs.writeFile(file2Path, 'file2.txt created', (err) => {
            if (err) {
                reject(err)
            } else {
                resolve('file2.txt')
            }
        })
    })

    return Promise.all([promis1, promis2])
}


// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
function deleteFileAfterTwoSec(fileNames) {
    const filePath1 = path.join(__dirname, fileNames[0])
    const filePath2 = path.join(__dirname, fileNames[1])
    new Promise((resolve, reject) => {
        fs.unlink(filePath1, err => {
            if (err) {
                reject(err)
            } else {
                resolve(`file1.txt deleted`)
            }
        })

    })
        .then(res => {
            console.log(res)
            fs.unlink(filePath2, err => {
                if (err) {
                    console.error(err)
                } else {
                    console.log(`file2.txt deleted`)
                }
            })
        })
        .catch(err => {
            console.error(err)
        })
}

createTwoFiles()
    .then(files => {
        return files
    })
    .then(files => {
        setTimeout(() => {

            deleteFileAfterTwoSec(files)
        }, 2000)
    })
    .catch(err => {
        console.error(err)
    })


// Q2. Create a new file with lipsum data (you can google and get this).
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining

function createNewFile(file, data) {
    const filePath = path.join(__dirname, file)
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, err => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

function readFile(fileName) {
    const filePath = path.join(__dirname, fileName)

    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}
function createFile(fileName, data) {
    const filePath = path.join(__dirname, fileName)

    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(`${fileName} created`)
            }
        })
    })
}
fetch('https://lipsum.com/feed/json')
    .then(response => response.json())
    .then(data => {
        const lipsumText = data.feed.lipsum
        return createNewFile('lipsum.txt', lipsumText)
    })
    .then(data => {
        return readFile('lipsum.txt', data)
    })
    .then(data => {
        return createFile('newlipsum.txt', data)
    })
    .then(() => {
        const filePath = path.join(__dirname, 'lipsum.txt')
        fs.unlink(filePath, err => {
            if (err) {
                console.log(err)
            } else {
                console.log('lipsum.txt deleted')
            }
        })
    })
    .catch(error => {
        console.log(error)
    })




function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user)
    } else {
        return Promise.reject(new Error("User not found"))
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    const filePath= path.join(__dirname,'activityFile.txt')
    return new Promise((resolve, reject) => {
        fs.appendFile(filePath,activity,err=>{
            if(err){
                reject(err)
            }else{
                resolve('activity added')
            }
        })
    })
}


// Q3.
// Use appropriate methods to 
// A. login with value 3 and call getData once login is successful
const user ={
    name:'deepak'
}
login(user,3)
.then(res=>{
    console.log(res)
})
.catch(err=>{
    console.log(err)
})


// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
login(user,2)
.then(res=>{
    return res
})
.then(user=>{

   return logData(user,`{date:${new Date()}}`)
})
.then((res)=>{

//     "Login Failure"
    return Promise.resolve(login(user,5))

})
.then(res=>{

   return logData(user,`{date:${new Date()}}`)
})
.then(res=>{
    //     "GetData Success"
   return getData()
})
.then(res=>{

   return logData(user,`{date:${new Date()}}`)
})
.then(res=>{
    return Promise.reject(getData())
})
.catch(err=>{
    console.error('err2:-',err)
})



//     "GetData Failure"

//     Call log data function after each activity to log that activity in the file.
//     All logged activity must also include the timestamp for that activity.
    
//     You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
//     All calls must be chained unless mentioned otherwise.
    
